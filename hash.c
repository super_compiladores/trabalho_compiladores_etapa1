/* Bibliotecas */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/** ----------- */
/** Constantes  */ #define SIZE_ELEMENTS 100
/**  Hashtable  */ #define STRING_LENGTH 1000
/** ----------- */ #define NOMEARQUIVO "trab.txt"

struct No{
    char *text;   // palavra
    int type;
    struct No *next;
};
typedef struct No no;

struct Element{
    int hashCode;
    struct No *no;      /* ponteiro para o nodo */
};
typedef struct Element element;

struct HashTable{
    long size;           /* tamanho tabela */
    struct Element **elements;
};
typedef struct HashTable hashtable;

hashtable* initializeHash(long tamanhoHash);
int *insertHash(hashtable *hash, char *text, int type, long tamanhoHash);
int computeHash(char text[], long tamanhoHash);
int searchHash(hashtable *hash, char *key, long tamanhoHash);
void listHash(hashtable *hash,long tamanhoHash);

void main(){

	hashtable *hash;
    hash = initializeHash(1000);

	printf("chegamos aqui\n");
	insertHash(hash,"ola",1, 1000);
	insertHash(hash,"olazin",4, 1000);
	insertHash(hash,"oha",2, 1000);

	printf("chegamos aqui 2\n");

    listHash(hash, 1000);
}


hashtable* initializeHash(long tamanhoHash){
    hashtable *hash;
    int i;
    if( tamanhoHash < 1 ) return NULL;
    else if(( hash = malloc( sizeof( hashtable ) ) ) == NULL ) {
		return NULL;
    }
    else{
        hash->elements = (struct element**) malloc(tamanhoHash * sizeof(struct element* ));
        if(hash->elements == NULL){
            free(hash);
            return NULL;
        }
        hash->size = tamanhoHash;
        for(i=0; i<tamanhoHash; i++){
            hash->elements[i] = NULL;
        }
        return hash;
    }
}



int *insertHash(hashtable *hash, char *text, int type, long tamanhoHash){

     no *noLista;
     int i,pos;

     if(hash == NULL)
        return 0;

     //converterMinuscula(text);
     pos = computeHash(text,tamanhoHash);

     // Caso seja a inser��o do primeiro nodo
     if(hash->elements[pos] == NULL){
        hash->elements[pos] = (struct element*)malloc(sizeof(element));
        hash->elements[pos]->hashCode = pos;
        hash->elements[pos]->no = (struct no*)malloc(sizeof(no));
        hash->elements[pos]->no->text = calloc(strlen(text) + 1, sizeof(char));
        strcpy(hash->elements[pos]->no->text, text);
        hash->elements[pos]->no->type = type;
        hash->elements[pos]->no->next = NULL;

    }else{

        noLista = hash->elements[pos]->no;
        if(searchHash(hash, text,tamanhoHash) == 1){
            while(noLista != NULL ){
            noLista = noLista->next;
            }
        }
        else{
            while(noLista->next != NULL){
                noLista = noLista->next;
            }
            noLista->next =  (struct no*)malloc(sizeof(no));
            strcpy(noLista->next->text, text);
            noLista->next->type = type;
            noLista->next->next =NULL;

        }
    }
    return 1;
}


/*------------------------------------------------------int searchHash(hashtable *hash, char *key, long tamanhoHash)-
-   Dada a tabela hash e uma chave, esta � procurada naquela.
-    -- Caso encontre: retorna 1;
-    -- Caso contr�rio: retorna 0;
-------------------------------------------------------*/
int searchHash(hashtable *hash, char *text, long tamanhoHash){
    int i=0, hashCode;

    no* newNo;
    hashCode = computeHash(text,tamanhoHash);

    if(hash->elements[hashCode] != NULL){
        if(hash->elements[hashCode]->no == NULL)
            return 0;

        newNo = hash->elements[hashCode]->no;

        while(newNo != NULL ){
            if(strcmp(text, newNo->text) == 0){ /* procura a chave na tabela hash */
                return 1;
            }
            newNo = newNo->next;
        }
    }
     return 0;
}

int computeHash(char text[], long tamanhoHash) {
    int h = 0,i=0, length_text;
    length_text = strlen(text);

    for (i = 0; i < length_text ; i++) {
        h = h + (int)(text[i]); // retorna o valor do caractere, na tabela ASCII
    }
    return h % tamanhoHash;
}

/*-------------------------------------------------------
-   Imprime a tabela hash.
-------------------------------------------------------*/
void listHash(hashtable *hash,long tamanhoHash){
    int i=0;

     for(i=0; i< tamanhoHash; i++){
        if(hash->elements[i] != NULL){
            printf("\n");
            printf("Hashcode: %d -----\n", hash->elements[i]->hashCode);
            no *newNo = hash->elements[i]->no;
            do{
                printf("Text: %s\n", newNo->text);
                printf("Type: %d\n", newNo->type);
                newNo = newNo->next;
            }while(newNo != NULL);
        }
    }
    getch();
}

