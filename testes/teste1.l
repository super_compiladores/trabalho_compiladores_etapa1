%{

// Compiladores 2020.1

#include <stdio.h>
#include <stdlib.h>
#include "tokens.h"
#include "hash.c"
int Upper=0;
int Lower=0;

%}

%%

[A-Z] {printf("Uppercase\t");Upper++;}
[a-z] {printf("Lowercase\t");Lower++;}
%%

int yywrap()
{
return 1;
}

void main()
{
printf("Enter a string\n");
yylex();

printf("Uppercase=%d and Lowercase=%d",Upper,Lower);
}
